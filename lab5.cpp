/*******************
  John Choi
  jschoi
  Lab 5
  Lab Section: 5
  Hollis Liu, Alex Meyers
*******************/
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/
   //Creation of Card structs for deck
   Card deck[52];

   int i = 0; int j = 0; int dcounter = 0;
   //Loop initializing the struct array with appropriate amount of
   //suits and card values.
   for(i = 0; i < 4; i++) {
     for(j = 2; j < 15; j++) {
       deck[dcounter].suit = static_cast<Suit>(i);
       deck[dcounter].value = j;
       dcounter++;
     }
   }
   //Declaration and initialization of function pointers to pass in later
   //function calls
   int (*randfp)(int);
   bool (*sofp)(const Card&, const Card&);

   sofp = &suit_order;
   randfp = &myrandom;

  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
   //Calling of random_shuffle with deck passed in along with a function pointer
   random_shuffle(deck, deck + 52, randfp);

   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
    //Initializing deckHand with first 5 cards of shuffled deck
    Card deckHand[5] = {deck[0], deck[1], deck[2], deck[3], deck[4]};

    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/
     //Calling of sort function with deckHand array at beginning, and at end
     // as parameters along with a function pointer to suit_order function
     sort(deckHand, deckHand + 5, sofp);


    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */
    //Variable k used for for loop
    int k = 0;
    //For loop outputting formatted card names and values to terminal.
    for(k = 0; k < 5; k++) {
      cout << setw(10) << get_card_name(deckHand[k]) <<
      get_suit_code(deckHand[k]) << endl;
    }

  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/

//suit_order takes in two arguments:
//1st parameter:  lhs is a pointer to a Card struct for comparison vs rhs
//2nd parameter:  rhs is a pointer to a Card struct for comparison vs lhs
//suit_order returns true or false boolean value depending on which Card
//struct has the higher precedence enumeration data member.  If the values
//are equal, the function compares the int data member on both Cards and
//returns true or false based on comparisons
bool suit_order(const Card& lhs, const Card& rhs) {
  if( lhs.suit < rhs.suit) {
    return true;
  }
  else if(lhs.suit > rhs.suit) {
    return false;
  }
  else if(lhs.suit == rhs.suit) {
    if(lhs.value < rhs.value) {
      return true;
    }
    else {
      return false;
    }
  }
  else {
    return true;
  }
}


//Function already written.
string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

//get_card_name takes in a pointer to a Card struct and returns string back
//depending on values of c's value data member.
//1st parameter:  pointer to Card struct c
//Using a switch statement, depending on the value of c, the corresponding
//string with the value will be returned as a string.
string get_card_name(Card& c) {
  switch(c.value) {
    case 2:   return "2 of ";
    case 3:   return "3 of ";
    case 4:   return "4 of ";
    case 5:   return "5 of ";
    case 6:   return "6 of ";
    case 7:   return "7 of ";
    case 8:   return "8 of ";
    case 9:   return "9 of ";
    case 10:  return "10 of ";
    case 11:  return "Jack of ";
    case 12:  return "Queen of ";
    case 13:  return "King of ";
    case 14:  return "Ace of ";
    default:  return "";
  }
}
